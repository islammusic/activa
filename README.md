SpreeActiva
===========

Spree Activa Gateway integration

Installation
------------

Add spree_activa to your Gemfile:

```ruby
gem 'spree_activa', git: 'https://bitbucket.org/islammusic/activa.git'
```

Configure via admin backend

Configuration/Payment methods

ignore gateway settings: SERVER, AUTO CAPTURE

NOTE: while testing make sure website url is accessible